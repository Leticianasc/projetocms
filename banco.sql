DROP SCHEMA IF EXISTS bd;
CREATE SCHEMA IF NOT EXISTS bd;

CREATE TABLE bd.cadastro(
	id INT (5) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	nome VARCHAR (50) NOT NULL,
	email VARCHAR (100) NOT NULL,
	senha VARCHAR (50) NOT NULL 
);

CREATE TABLE bd.cimg(
	id INT (5) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	name VARCHAR (255) NOT NULL,
	tipo VARCHAR (255) NOT NULL,
	bin LONGBLOB NOT NULL
);

CREATE TABLE bd.site(
	id INT (5) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	name VARCHAR (255) NOT NULL,
	slog VARCHAR (255) NOT NULL
);

CREATE TABLE bd.card1(
	id INT (5) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	legenda VARCHAR (255) NOT NULL
);

CREATE TABLE bd.card2(
	id INT (5) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	legenda VARCHAR (255) NOT NULL
);

CREATE TABLE bd.card3(
	id INT (5) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	legenda VARCHAR (255) NOT NULL
);

CREATE TABLE bd.card4(
	id INT (5) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	legenda VARCHAR (255) NOT NULL
);

CREATE TABLE bd.card5(
	id INT (5) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	legenda VARCHAR (255) NOT NULL
);

CREATE TABLE bd.card6(
	id INT (5) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	legenda VARCHAR (255) NOT NULL
);