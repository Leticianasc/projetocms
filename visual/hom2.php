<?php
echo"
<!DOCTYPE html>
<html lang='pt-br'>
<head>
  <meta name='charset' content='utf-8'/>
  <meta name='viewport' content='width-device-width, initial-scale-1.0' />
  <title>Bootstrap</title>

  <link href='css/Bootstrap.min.css' rel='stylesheet' type='text/css' />
  <script type='text/javascript' src='js/jquery.js'></script>
  <script type='text/javascript' src='js/Bootstrap.min.js'></script>

  <link href='css/home2.css' rel='stylesheet' type='text/css' />

</head>
<body id='page-top'>

   
    <nav class='navbar navbar-expand-lg navbar-dark bg-dark fixed-top' id='mainNav'>
      <div class='container'>
        <a class='navbar-brand js-scroll-trigger' href='#page-top'>AIRAM MOVIE</a>
        <button class='navbar-toggler' type='button' data-toggle='collapse' data-target='#navbarResponsive' aria-controls='navbarResponsive' aria-expanded='false' aria-label='Toggle navigation'>
          <span class='navbar-toggler-icon'></span>
        </button>
        <div class='collapse navbar-collapse' id='navbarResponsive'>
          <ul class='navbar-nav ml-auto'>
            <li class='nav-item'>
              <a class='nav-link js-scroll-trigger' href='#page-top'>Home</a>
            </li>
            <li class='nav-item'>
            
              <a class='nav-link js-scroll-trigger' href='#about'>Filme e Séries</a>
            </li>
            <li class='nav-item'>
              <a class='nav-link js-scroll-trigger' href='login.php'>Modificar</a>
            </li>
            
           
          </ul>
        </div>
      </div>
    </nav>

    <header class='killimanjaro-header text-white'>
      <div class='container text-center'>
        <h1>Veja nossos últimos lançamentos</h1>
        <p class='lea'>de filmes e séries.</p>
      </div>
    </header>
    
    <section id='about'>
    <div class='container text-center'>
        <h1>Filmes e Séries que estão em alta</h1> <p class='lead'>Assista já!</p>
       
      </div>

       <div class='album py-5 bg-light'>
        <div class='container'>


          <div class='row'>
            <div class='col-md-4'>
              <div class='card mb-4 shadow-sm'>
              <img class='card-img-top' src='css/Segredosdenatal.jpg' alt='Card image cap'>
                <div class='card-body'>
                  <p class='card-text'>Segredos de Natal</p>
                   <p class='card-text'>Uma reunião de Natal faz uma viagem ao passado nesta série dividida em três partes,que explora as complexidades da história de uma família.</p>
                  <div class='d-flex justify-content-between align-items-center'>
                    <div class='btn-group'>
                      
                     
                    </div>
                    
                  </div>
                </div>
              </div>
            </div>
            <div class='col-md-4'>
              <div class='card mb-4 shadow-sm'>
           <img class='card-img-top' src='css/bussoladeouro.jpg' alt='Card image cap'>
                <div class='card-body'>
                  <p class='card-text'>Bússola de Ouro</p>
                   <p class='card-text'>A órfã Lyra troca sua vida sem preocupações na faculdade por uma aventura de outro mundo no extremo norte.</p>
                  <div class='d-flex justify-content-between align-items-center'>
                    <div class='btn-group'>
                      
                     
                    </div>
                    
                  </div>
                </div>
              </div>
            </div>
            <div class='col-md-4'>
              <div class='card mb-4 shadow-sm'>
    <img class='card-img-top' src='css/valoz.jpg' alt='Card image cap'>
                <div class='card-body'>
                  <p class='card-text'>Velozes e Furiosos 8</p>
                   <p class='card-text'>Traídos por alguém próximo, eles cruzarão o mundo para trazê-lo de volta, com direito a perseguições eletrizantes.
                   <br>
                   <br>

                   </p>
                  <div class='d-flex justify-content-between align-items-center'>
                    <div class='btn-group'>
                      
                     
                    </div>
                    
                  </div>
                </div>
              </div>
            </div>

            <div class='col-md-4'>
              <div class='card mb-4 shadow-sm'>
    <img class='card-img-top' src='css/esq.png' alt='Card image cap'>
                <div class='card-body'>
                  <p class='card-text'>Esquadrão suicida</p>
                   <p class='card-text'>A reunião dos vilões mais loucos e incorrigíveis é uma missão suicida. Talvez não para eles.
                   <br>
                   <br>
                   <br>
                   </p>
                  <div class='d-flex justify-content-between align-items-center'>
                    <div class='btn-group'>
                      
                     
                    </div>
                    
                  </div>
                </div>
              </div>
            </div>
            <div class='col-md-4'>
              <div class='card mb-4 shadow-sm'>
    <img class='card-img-top' src='css/fill.png' alt='Card image cap'>
                <div class='card-body'>
                  <p class='card-text'>Relâmpago McQueen</p>
                   <p class='card-text'>Nesta animação, o promissor carro de corridas Relâmpago McQueen atravessa o país para participar de uma corrida importante, mas acaba se desviando pelo caminho. </p>
                  <div class='d-flex justify-content-between align-items-center'>
                    <div class='btn-group'>
                      
                     
                    </div>
                    
                  </div>
                </div>
              </div>
            </div>
            <div class='col-md-4'>
              <div class='card mb-4 shadow-sm'>
    <img class='card-img-top' src='css/musica.jpg' alt='Card image cap'>
                <div class='card-body'>
                  <p class='card-text'>A Última Música</p>
                  <p class='card-text'>A adolescente Ronnie não fica nada satisfeita quando a mãe decide enviá-la a uma cidade litorânea para passar o verão com o pai, que não vê há anos. Enquanto o pai tenta se reconectar com a filha.</p>
                  <div class='d-flex justify-content-between align-items-center'>
                    <div class='btn-group'>
                      
                     
                    </div>
                    
                  </div>
                </div>
              </div>
            </div>

            <div class='col-md-4'>
              <div class='card mb-4 shadow-sm'>
    <img class='card-img-top' src='css/passarooriente.jpg' alt='Card image cap'>
                <div class='card-body'>
                  <p class='card-text'>Pássaro do Oriente</p>
                  <p class='card-text'>Na tóquio dos anos 80, uma estrangeiras misteriosa é suspeita de assassinar uma amiga, desaparecida após as duas se envolverem em um triângulo amoroso.</p>
                  <div class='d-flex justify-content-between align-items-center'>
                    <div class='btn-group'>
                      
                     
                    </div>
                    
                  </div>
                </div>
              </div>
            </div>
            <div class='col-md-4'>
              <div class='card mb-4 shadow-sm'>
    <img class='card-img-top' src='css/zerandoavida.png' alt='Card image cap'>
                <div class='card-body'>
                  <p class='card-text'>Zerando a Vida</p>
                  <p class='card-text'>Quando seu amigo do peito forja sua morte e te leva para passear de iate, aguarde outras surpresas.
                  <br>
                  <br>
                  </p>
                  <div class='d-flex justify-content-between align-items-center'>
                    <div class='btn-group'>
                      
                     
                    </div>
                    
                  </div>
                </div>
              </div>
            </div>
            <div class='col-md-4'>
              <div class='card mb-4 shadow-sm'>
    <img class='card-img-top' src='css/seteminutos.jpg' alt='Card image cap'>
                <div class='card-body'>
                  <p class='card-text'>Sete Minutos Depois da Meia-Noite</p>
                  <p class='card-text'>Abalado pela doença da mãe, um jovem garoto começa a entender a complexidade do ser humano através das fantásticas histórias do monstro da árvore.</p>
                  <div class='d-flex justify-content-between align-items-center'>
                    <div class='btn-group'>
                      
                     
                    </div>
                    
                  </div>
                </div>
              </div>
            </div>
            <br>
            
             <div class='col-md-4'>
              <div class='card mb-4 shadow-sm'>
           <img class='card-img-top' src='css/Anne-With-an-e.jpg' alt='Card image cap'>
                <div class='card-body'>
                  <p class='card-text'>Anne With An E</p>
                  <p class='card-text'>Neste filme baseado no livro 'Anne de Green Gables', uma impetuosa órfã é adotada por engano por um casal de irmãos solteirões do interior. </p>
                  <div class='d-flex justify-content-between align-items-center'>
                    <div class='btn-group'>
                      
                     
                    </div>
                    
                  </div>
                </div>
              </div>
            </div>
             <div class='col-md-4'>
              <div class='card mb-4 shadow-sm'>
           <img class='card-img-top' src='css/danceacademy.jpg' alt='Card image cap'>
                <div class='card-body'>
                  <p class='card-text'>Dance Academy </p>
                  <p class='card-text'>A jovem Tara realiza seu sonho ao ser aceita em uma prestigiosa academia de dança, mas ela logo percebe que dançar é apenas parte da batalha.</p>
                  <div class='d-flex justify-content-between align-items-center'>
                    <div class='btn-group'>
                      
                     
                    </div>
                    
                  </div>
                </div>
              </div>
            </div>
            <div class='col-md-4'>
              <div class='card mb-4 shadow-sm'>
           <img class='card-img-top' src='css/Irmandade.png' alt='Card image cap'>
                <div class='card-body'>
                  <p class='card-text'>Irmandade</p>
                   <p class='card-text'>Uma advogada honesta enfrenta um dilema moral depois que policiais a forçam a delatar seu irmão, que está preso e lidera uma facção criminosa em ascensão.</p>
                  <div class='d-flex justify-content-between align-items-center'>
                    <div class='btn-group'>
                      
                     
                    </div>
                    
                  </div>
                </div>
              </div>
            </div>
            <div class='col-md-4'>
              <div class='card mb-4 shadow-sm'>
           <img class='card-img-top' src='css/orange-is-the-new-black-.jpg' alt='Card image cap'>
                <div class='card-body'>
                  <p class='card-text'>Orange is the new black</p>
                  <p class='card-text'>Condenada a 15 meses de prisão por um crime cometido em sua juventude, Piper Chapman deixa Larry, seu atencioso noivo, por um novo lar: a penitenciária feminina.</p>
                  <div class='d-flex justify-content-between align-items-center'>
                    <div class='btn-group'>
                      
                     
                    </div>
                    
                  </div>
                </div>
              </div>
            </div>
            <div class='col-md-4'>
              <div class='card mb-4 shadow-sm'>
           <img class='card-img-top' src='css/visavis.jpg' alt='Card image cap'>
                <div class='card-body'>
                  <p class='card-text'>VISaVIS</p>
                  <p class='card-text'>Macarena fica arrasada ao ser condenada a sete anos de prisão por crimes fiscais e tenta ocultar este fato dos pais.
                  <br>
                  <br>
                  </p>
                  <div class='d-flex justify-content-between align-items-center'>
                    <div class='btn-group'>
                      
                     
                    </div>
                    
                  </div>
                </div>
              </div>
            </div>
            <div class='col-md-4'>
              <div class='card mb-4 shadow-sm'>
           <img class='card-img-top' src='css/batesmotel.jpg' alt='Card image cap'>
                <div class='card-body'>
                  <p class='card-text'>Bates Motel</p>
                  <p class='card-text'>Após a morte do pai, Norman Bates e sua mãe abrem um hotel. Mas o estado mental de Norman os leva por um caminho sombrio e violento.</p>
                  <div class='d-flex justify-content-between align-items-center'>
                    <div class='btn-group'>
                      
                     
                    </div>
                    
                  </div>
                </div>
              </div>
            </div>
           
           
          </div>
        </div>
      </div>

    </section>
    <section id='services-banner' class='bg-light'>
      <div class='container'>
        <div class='row'>
         <div class='card' style='width: 18rem;'>
  <img src='css/undraw_personal_site_xyd1.png' class='card-img-top' alt='...'>
  <div class='card-body'>
    <p class='card-text'>O site oferece novos lançamentos de filmes e séries, responsividade, entre outros meios didáticos.</p>
  </div>
</div>
        </div>
      </div>
    </section>
    <section id='services' class='bg-light'>
      <div class='container'>
        <div class='row'>
            <div class='col-sm-8'>

          <div class='card bg-dark text-white'>
  <img class='card-img' src='css/rei-leão.jpg' alt='Card image'>
  <div class='card-img-overlay'>
    <h5 class='card-title'></p>
  </div>
</div></div>
<div class='col-sm-4'>

<div class='card' style='width: 18rem;'>
  <div class='card-body'>
    <h5 class='card-title'>Site com entrentedimento feito para você.</h5>
    <a href='#' class='btn btn-primary'>Home</a>
    
   
  </div></div>
</div>
        </div>
      </div>
    </section>
<section id='services-word' class='bg-light'>
      <div class='container'>
        <div class='row'>
      
      </div>

   
      
  <section id='contact footer-bottom'>
   <footer class='container py-5'>
      <div class='row'>
        <div class='col-12 col-md' id='p'>
          <h3>Sobre nós</h3>
          <p>Esse site é pensado em você, que adora assistir filmes, fique atualizado nos nossos lançamentos de filmes e séries em alta e assista com sua família.</p>
        </div>
       <div class='card border-danger mb-3' style='max-width: 18rem;'>
  <img src='css/pipoca.jpg' class='card-img-top' alt='foto'>
  <div class='card-body text-danger'>
   
    <p class='card-text'>Aqui você verá novos lançamentos de filmes e séries.</p>
  </div>
  
</div>
<div class='card border-warning mb-3' style='max-width: 18rem;'>
 
  <img src='css/pi.jpg' class='card-img-top' alt='foto'>
  <div class='card-body text-warning'>
  
    <p class='card-text'>Fique ligado!</p>

  </div>
  
</div>

      </div>
    </footer>
  
</section>
 </section>
<section id='services-word' class='bg-light'>
      <div class='container'>
        <div class='row'>
      
      </div>
<div class='card-deck'>
  <div class='card'>
    <img src='css/harry.jpg' class='card-img-top' alt='foto'>
    <div class='card-body'>
      <h5 class='card-title'>Harry Potter</h5>
      <p class='card-text'>“Não são as nossas habilidades que mostram o que realmente somos… são nossas escolhas” – Dumbledore.</p>
      
    </div>
  </div>
  <div class='card'>
    <img src='css/ben.jpg' class='card-img-top' alt='foto'>
    <div class='card-body'>
      <h5 class='card-title'>O Curioso Caso de Benjamin Button</h5>
      <p class='card-text'>“A vida só pode ser compreendida olhando para trás, mas só pode ser vivida olhando para frente.”</p>
      
    </div>
  </div>
  <div class='card'>
    <img src='css/procura.jpg' class='card-img-top' alt='foto'>
    <div class='card-body'>
      <h5 class='card-title'>À Procura da Felicidade</h5>
      <p class='card-text'>“Nunca deixe que alguém lhe diga que não pode fazer algo. Se você tem um sonho, tem que protegê-lo. As pessoas que não podem fazer por si mesmas, dirão que você não consegue. Se quer alguma coisa, vá e lute por ela. Ponto final.” </p>
      
    </div>
  </div>
</div>
   
   

</body>
</html>
";