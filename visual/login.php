<?php

echo"
<!DOCTYPE html>
<html lang='pt-br'>
<head>
	<meta name='charset' content='utf-8'/>
	<meta name='viewport' content='width-device-width, initial-scale-1.0' />
	<title>Bootstrap</title>

	<link href='css/Bootstrap.min.css' rel='stylesheet' type='text/css' />
	<script type='text/javascript' src='js/jquery.js'></script>
	<script type='text/javascript' src='js/Bootstrap.min.js'></script>

	<link href='css/style.css' rel='stylesheet' type='text/css' />

</head>
<body>

	<section class='login-block'>
	    <div class='container'>
		<div class='row'>
			<div class='col-md-4 login-sec'>
			    <h2 class='text-center'>Login</h2>


	  <form action='../controle/veri-login.php' method='post'>
	  <div class='form-group'>
	    <label class='text'>Usuário</label>
	    <input type='text' class='form-control' name='nome' required placeholder='Nome'>
	    
	  </div>
	  <div class='form-group'>
	    <label class='text'>Password</label>
	    <input type='password' class='form-control' name='senha' required placeholder='Senha'>
	  </div>
	  
	  
	    <div class='form-group'>
	  </br>
	    <button type='submit' class='btn btn-login float-right'>Acessar</button>
	     <a href='cadastro.php'> Cadastrar </a>
	  
	  </div>
</form>
	  
	
	
			</div>
			<div class='col-md-8 banner-sec'>
	            <div id='carouselExampleIndicators' class='carousel slide' data-ride='carousel'>
	                 <ol class='carousel-indicators'>
	                    <li data-target='#carouselExampleIndicators' data-slide-to='0' class='active'></li>
	                    <li data-target='#carouselExampleIndicators' data-slide-to='1'></li>
	                    <li data-target='#carouselExampleIndicators' data-slide-to='2'></li>
	                  </ol>
	            <div class='carousel-inner' role='listbox'>
	    <div class='carousel-item active'>	

	      <img class='d-block img-fluid' src='css/filmes.jpg' >
	      <div class='carousel-caption d-none d-md-block'>
	        <div class='banner-text'>
	            <h2>Bem vindo</h2>
	            <p> Ao site de entretenimento voltado para filmes e séries.</p>
	        </div>	
	  </div>
	    </div>
	    <div class='carousel-item'>
	      <img class='d-block img-fluid' src='css/vel.jpg' >
	      <div class='carousel-caption d-none d-md-block'>
	        <div class='banner-text'>
	            <h2>Fique ligados nos filmes atuais</h2>
	            <p>Nos últimos lançamentos.</p>
	        </div>	
	    </div>
	    </div>
	    <div class='carousel-item'>
	      <img class='d-block img-fluid' src='css/6-ss.jpg' >
	      <div class='carousel-caption d-none d-md-block'>
	        <div class='banner-text'>
	            <h2>Crie seu próprio site</h2>
	            <p>E se junte a nós.</p>
	        </div>	
	    </div>
	  </div>
	            </div>	   
			    
			</div>
		</div>
	</div>
	</section>
	
</body>
</html>

";