<?php
require_once("Conect.php");
require_once("../modelo/usuariomodelo.php");
class UsuarioControle{
            function selecionarTodos(){
                try{
                    $conexao = new Conexao();
                    $cmd = $conexao->getConexao()->prepare("SELECT nome,senha FROM cadastro");
                    $cmd->execute();
                    $resultado = $cmd->fetchAll(PDO::FETCH_CLASS, "Usuario");
                    return $resultado;
                }catch(PDOException $e){
                    echo "Erro no banco: {$e->getMessage()}";
                }catch(Exception $e){
                    echo "Erro geral: {$e->getMessage()}";
                }
            }
            function verificarLogin($nome,$senha){
                try{
                    $conexao = new Conexao();
                    $cmd = $conexao->getConexao()->prepare("SELECT nome,senha FROM cadastro WHERE nome = :n AND senha = :s");
                    $cmd->bindParam("n",$nome);
                    $cmd->bindParam("s",$senha);
                    if($cmd->execute()){
                    if( $cmd->rowCount() == 1){
                        return true;
                    }else{
                        return false;
                    }
                }
                }catch(PDOException $e){
                    echo "Erro no banco: {$e->getMessage()}";
                }catch(Exception $e){
                    echo "Erro geral: {$e->getMessage()}";
                }
            }
        }
?>